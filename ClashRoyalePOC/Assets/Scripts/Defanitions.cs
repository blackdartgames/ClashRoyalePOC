﻿using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public enum BuilidingType { Base, Tower }
public enum AttackerType { Hero, Soldier}
[System.Serializable]
public class HealthNotifyEvent : UnityEvent<float> { }

public enum Player { Home, Away }
public enum TargetType { Moving, Static }

public interface ITargetFoundNotifier
{
    void TargetAcquired(Target target);
}

public delegate void OnRefreshTarget();
public interface IObservable<T>
{
    void AddObserver(T attackPower);
    void RemoveObserver(T boostPower);
}

public interface ILife
{
    void HitRecived(float attackPower);
    void BoostRecived(float boostPower);
}

public interface ITargetStateNotifier
{
    void AddNewTarget(Target target);
    void RemoveTarget(Target target);
    bool SearchTarget(Target target);
    bool SelectTargetInRange(Target target,float range);
}

public interface IDestroyed
{    
    void OnDestroyed(BuilidingType building, Player player);
}

public interface IGameOver
{
    void OnGameCompleted();
}
    