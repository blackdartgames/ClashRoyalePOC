﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;


public class TargetManager : MonoBehaviour, ITargetStateNotifier
{
    private Dictionary<Player, List<Target>> mTargetPool;
    private Queue<IEnumerator> mProcessQueue;
    private bool mQueueProcessing = false;
    private IEnumerator mActiveProcess;

    void Awake()
    {
        mTargetPool = new Dictionary<Player, List<Target>>();
        mTargetPool.Add(Player.Home, new List<Target>());
        mTargetPool.Add(Player.Away, new List<Target>());
        mProcessQueue = new Queue<IEnumerator>();
    }

    void Update()
    {
        if (!mQueueProcessing && mProcessQueue.Count > 0)
        {
            StartCoroutine(mProcessQueue.Dequeue());
        }
    }

    public void AddNewTarget(Target target)
    {
        if (!mTargetPool[target._Player].Exists((x) => x.mTargetId == target.mTargetId))
        {
            mTargetPool[target._Player].Add(target);
        }
    }

    public void RemoveTarget(Target target)
    {
        mTargetPool[target._Player].RemoveAll((x) => x.mTargetId == target.mTargetId);
    }

    public bool SearchTarget(Target target)
    {
        switch (target._Player)
        {
            case Player.Home:
                if (mTargetPool[Player.Away].Count > 0)
                {
                    mProcessQueue.Enqueue(GetTarget(Player.Away, target));
                    return true;
                }
                break;
            case Player.Away:
                if (mTargetPool[Player.Home].Count > 0)
                {
                    mProcessQueue.Enqueue(GetTarget(Player.Home, target));
                    return true;
                }
                break;
        }
        return false;
    }

    public bool SelectTargetInRange(Target target,float range)
    {
        switch (target._Player)
        {
            case Player.Home:
                if (mTargetPool[Player.Away].Count > 0)
                {
                    mProcessQueue.Enqueue(GetTarget(Player.Away, target));
                    return true;
                }
                break;
            case Player.Away:
                if (mTargetPool[Player.Home].Count > 0)
                {
                    mProcessQueue.Enqueue(GetTarget(Player.Home, target));
                    return true;
                }
                break;
        }
        return false;
        return false;
    }

    public IEnumerator RefreshTargets(TargetType type)
    {
        foreach (var player in mTargetPool)
	    {
            for (int j = 0; j < player.Value.Count; j++)
            {
                if (player.Value.Count > j)
                {
                    player.Value[j].RefreshTarget();
                    yield return null;
                }
            }
	    }
        yield break;
    }

    IEnumerator GetTarget(Player player, Target target)
    {
        mQueueProcessing = true; 
        if (target != null)
        {
            target.TargetAcquired(mTargetPool[player].OrderBy((x) => Vector3.Distance(target.transform.position, x.transform.position)).First());
        }
        mQueueProcessing = false;
        yield break;
    }       

}





