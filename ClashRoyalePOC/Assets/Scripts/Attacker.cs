﻿using UnityEngine;
using System.Collections;

public abstract class Attacker : Target, IGameOver
{
    public float _Speed;
    public float _Range;
    public float _MinAttackDistance;
    public float _HitPoint;
    public float _SpawnDelay;
    public float _AttackDelay;
    [SerializeField]
    protected NavMeshAgent _NavAgent;
    protected virtual void WakeUpPlayer()
    {
        GameController.Instance.AddObserver(this);
        StartCoroutine(InvokeAttacker(_SpawnDelay));
    }
    protected abstract void UpdateDestination(Vector3 position);
    protected abstract void AttackTarget(ILife target);
    private IEnumerator InvokeAttacker(float time)
    {
        yield return new WaitForSeconds(time);
        mIsActive = true;
    }
    public void OnGameCompleted()
    {
        mIsActive = false;
        DestroyMe();
    }
    public override void DestroyMe()
    {
        GameController.Instance.RemoveObserver(this);
        base.DestroyMe();
    }
}
